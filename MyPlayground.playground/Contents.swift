//CLASS 1
//: Playground - noun: a place where people can play
import UIKit

//variable
var str = "Hello, playground"
//constante
let str2 = "Hello, playground 2"

str = "sonmething else"
//str2 = "something else 2" -> ERROR defined as let (constante)

// se puede usar emojis
let 😘 = "car"

//str = 3 -> ERROR int to string

//Booleans
let myBool = true
let myBool2 = false

let myInt = 5
let myDouble = 3.5

print("Hello")
print(str)
print(myBool)
print(myDouble)
print("My int is: \(myInt)")

let mySum = Double(myInt) + myDouble

//Tuples
let coordinates = (5,2)
let coordinatesDouble = (5.1, 2.5)
let coodinatesMixed = (5, 2.5)
let coordinates3 = (1, 5, 8)

coordinates.0

let (x, y, z) = (9, 4, 2)
x
y
z

//IF || && >= <= == !=
if x > 5 {
    print("it's greater")
}

//WHILE
var counter = 0
while counter < 10 {
    counter
    counter += 1
}

//DO WHILE
repeat {
    counter -= 1
} while counter > 0

//FOR
for _ in 1...5{
    print("hello")
}

for i in 1...5{
    print(i)
}

for i in 1...20 where i % 2 == 0{
    print(i)
}

//FOR EACH
let array = [1,2,3,4,5,6,7,8,9,65,4,342,2,2,3,3,5,6]

for n in array {
    print(n)
}

for n in array where n % 2 == 0{
    print(n)
}

//SWITCH
let age = 20

switch age {
case 0...17 where age % 2 == 0:
    print("underage")
case 18...64:
    print("is 18 between 64 = adult")
case 20:
    print("is 20")
default:
    print("other not recognized")
}

//CLASS 2
//FUNCIONES
//retorna void
func myFunction(){
    print("Hello")
}

myFunction()

//privada por clase
private func myPrivateFunction(){
    print("Hello from private")
}

//se hace privada solo por archivo y no por clase
fileprivate func myFilePrivateFunction(){
    print("Hello from file private")
}

func intFunction() -> Int {
    return 5
}

intFunction()

func calcFunction() -> (Int, Int){
    return (5+5, 5*5)
}

calcFunction()
calcFunction().0
calcFunction().1

func calcFunction2() -> (sum:Int, mult:Int){
    return (5+5, 5*5)
}

calcFunction2()
calcFunction2().mult
calcFunction2().sum


func calcFunction3() -> (sum:Int, mult:Int, Bool){
    return (5+5, 5*5, false)
}

func calcFunction4(n:Int) -> (sum:Int, mult:Int, Bool){
    return (5+5, 5*5, false)
}
var nx=5
calcFunction4(n: nx)
calcFunction4(n:5)

func addOne(number n:Int)->Int{
    return n+1
}
addOne(number: 4)

func add(_ n1:Int, _ n2:Int)->Int{
    return n1+n2
}
add(5, 6)

func addUopToFour(_ n1:Int, _ n2:Int, _ n3:Int = 0, _ n4:Int = 0)->Int{
    return n1+n2+n3+n4
}
addUopToFour(1, 2)
addUopToFour(1, 2, 3)
addUopToFour(1, 2, 3, 4)


//OPTIONALS
let myOptionalInt = Int("777")
print(myOptionalInt)

//FORMAS DE QUITAR OPTIONAL
//1era forma (no se debe usar solo si estamos 100% seguros)
print(myOptionalInt!)

//2da forma (la mas recomendada)
if let internalInt = myOptionalInt{
    print(internalInt)
}

//3era forma (elegante)
print(myOptionalInt ?? 0)

//4ta forma (solo funciona en funciones)
func unwrapOptional(n:Int?){
    guard let myInt = n else{
        return
    }
    print(myInt)
}

unwrapOptional(n: nil)
unwrapOptional(n: 4)


//error: myVar1 es un Int pero luego quiero un Double
//var myVar1 = 50
//myVar1 = 50.5

//solucion
var myVar2:Double = 50
myVar2 = 50.5


//COLLECTIONS
//ARREGLOS
//sirve cuando nos interesa el orden
var evenNumbers1 = [2, 4, 6, 8]
let evenNumbers2:Array<Int> = [4, 6, 8, 10]
let evenNumbers3:[Int] = [10, 12, 14]
let myArray:[Double] = []


//add values
//i
evenNumbers1.append(10)
evenNumbers1.append(12)
//ii
evenNumbers1 += [14]

evenNumbers1.isEmpty
evenNumbers1.count
evenNumbers1.first

if let first = evenNumbers1.first{
    print(first)
}else{
    print("Nothing to be printed")
}

evenNumbers1.max()
evenNumbers1.min()

evenNumbers1.contains(4)

//error
//print(myArray[0])

let customArray = evenNumbers1[0...2]

evenNumbers1.insert(90, at: 0)

//DICTIONARY o MAP
//sirve cuando vamos hacer muchas busquedas
var myScores = ["Sebas":10, "Adrian":12, "Andres":20]
myScores["Sebas"]
print(myScores["Sebas"])
print(myScores["Sebas"] ?? 0)
print(myScores["Cristian"] ?? 0)

myScores["Cristian"] = 80

for (player, score) in myScores{
    print("Player \(player) score is: \(score) ")
}


//SET: cuando no importa el orden se usa set
//sirve cuando solo nos interesa saber si esta el elemento ahi
var someSet:Set<Int> = [2,1,1,23,4,5,6,2]
someSet.contains(1)
someSet.isEmpty
someSet.insert(900)





//CLASS 3
//CLOSURES
var multiplyClosure: (Int, Int) -> (Int)

multiplyClosure = { (a:Int, b:Int) in
    return a * b
}
let result = multiplyClosure(3, 6)

//la misma de arriba
var multiplyClosure2: (Int, Int) -> Int = {$0 * $1}
let result2 = multiplyClosure2(5, 4)

//closure como parametro
func operateNumbers(
    _ a:Int,
    _ b:Int,
    operation:(Int, Int) -> (Int)
    ) -> Int {
    return operation(a,b)
}

operateNumbers(5, 3, operation: multiplyClosure)

operateNumbers(10, 2, operation: {$0/$1})


//SORT
var numbers = [1, 3, 5, 9, 5, 12, 10, 29, 39, 48, 27, 50]

//sort asc
numbers.sorted()

//sort desc
numbers.sort { (a, b) -> Bool in
    return a > b
}

//sort desc
//para usar este se debe tener arreglos unicamente tipo var
numbers.sort{$0 > $1}

//sort desc rapido
numbers.sorted().reversed()

//PROGRAMACION FUNCIONAL
let prices = [10.4, 11, 5.5, 2, 3, 4.8, 20]

//FILTER
//prog. imperativa
let filtered = prices.filter { (price) -> Bool in
    return price > 8
}
//prog. funcional
let filtered2 = prices.filter{$0 > 8}
filtered2

//MAP
let onSalePrices = prices.map{$0 * 0.9}
onSalePrices

//REDUCE
let sum = prices.reduce(0, {$0 + $1})
sum



//STRINGS
let name = "Sebas"
let message = "Hello, " + name
//para escribir strings en varias lineas """
let message2 = """
Super
super
super long
string
"""

for char in name {
    print(char)
}

name.count

let cafeNormal = "café"

//strings son UNICODE
let cafeComb = "cafe\u{0301}"
print(cafeComb)

//tienen el mismo numero de caracteres (caracteristica de swift que ahorra memoria)
cafeNormal.count
cafeComb.count


//STRUCTS VS CLASES
//struc hace paso por valor y las clases por referencia

//STRUCTS
struct Person{
    let firstName:String
    let lastName: String
    //compute property siempre var
    var fullName:String {
        get {
            return firstName + " " + lastName
        }
    }
    //corre solo cuando se cambia la variable
    var age:Int {
        didSet {
            if age < oldValue {
                age = oldValue
            }
            print("Age changed from \(oldValue) to \(age)")
        }
    }
    func printName(){
        print("My name is \(name)")
    }
}

let p1 = Person(firstName: "Sebas", lastName: "Guzmán", age: 26)
//se pasa por valor, es decir q son independientes
var p2 = p1

p1.fullName
p2.age = 30
//no deja por didset
p2.age = 25

p1.age
p2.age

p1.printName()


//CLASS
class PersonClass{
    let firstName:String
    let lastName:String
    var age:Int

    var fullName:String{
        return firstName + " " + lastName
    }
    
    init(firstName:String, lastName:String, age:Int){
        self.firstName = firstName
        self.lastName = lastName
        self.age = age
    }
}

let p3 = PersonClass(firstName: "Sebas2", lastName:"Guzman2", age:26)
let p4 = p3
p3.age
p4.age
p4.age = 50
p3.age
p4.age
